## Vanilla Node.js Project

This project is created using node version 20.X.X, so make sure you initialise the project accordingly.
If anyone are currently working in specific version of node js, you can even run the project inside the docker
container for which docker file is already created.

## NOTE: Remember to create a bind mount with a root directory before running the container so that you don't need to recreate the image everytime you change the code base.

## Getting started

Yes this project is based in complete raw node js, no framework. But to get the DB up nd running we need to have the corresponding DB driver. So there will be on dependency in our project. All you need to do is `npm install` if you are running it locally.
