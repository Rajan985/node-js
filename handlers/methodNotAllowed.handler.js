const HTTPCode = require("../constants/HTTPCode");

module.exports = (payload, cb) => {
  cb(
    {message: `${payload.method} method not allowed for this endpoint.`},
    HTTPCode.METHOD_NOT_ALLOWED
  );
};
