const HTTPCode = require("../constants/HTTPCode");
const {signJWT, verifyJWT} = require("../utils/jwt");
const crypto = require("crypto");
const {hashPassword} = require("../utils/passwordHasher");

class AuthHandler {
  constructor(db) {
    this.db = db;
    this.collection = "Users";
  }

  async signup(payload, cb) {
    const username =
      payload.requestPayload.username !== undefined &&
      payload.requestPayload.username.trim().length >= 5
        ? payload.requestPayload.username
        : false;

    const email =
      payload.requestPayload.email !== undefined &&
      payload.requestPayload.email.trim().length > 0
        ? payload.requestPayload.email
        : false;
    const password =
      payload.requestPayload.password !== undefined &&
      payload.requestPayload.password.trim().length >= 6
        ? payload.requestPayload.password
        : false;

    if (!username || !email || !password)
      return cb({message: "Insufficient data"}, HTTPCode.BAD_REQUEST);

    const salt = crypto.randomBytes(64).toString("hex");
    try {
      this.db.collection(this.collection).insertOne({
        username,
        password: hashPassword(password, salt),
        salt,
        email,
        created_at: new Date(),
        updated_at: new Date(),
      });

      cb({message: "User created successfully"}, HTTPCode.CREATED);
    } catch (err) {
      console.log(err);
    }
  }

  async login(payload, cb) {
    const email =
      payload.requestPayload.email !== undefined &&
      payload.requestPayload.email.trim().length > 0
        ? payload.requestPayload.email
        : false;

    const password =
      payload.requestPayload.password !== undefined &&
      payload.requestPayload.password.trim().length >= 6
        ? payload.requestPayload.password
        : false;

    if (!email || !password)
      return cb({message: "Insufficient data"}, HTTPCode.BAD_REQUEST);

    const user = await this.db.collection(this.collection).findOne({email});

    if (!user || user.password !== hashPassword(password, user.salt))
      return cb(
        {message: "Invalid username or password."},
        HTTPCode.UNAUTHORISED
      );

    delete user.password;
    delete user.salt;

    const token = signJWT({_id: user._id});

    cb({token, user}, 200);
  }

  test(payload, cb) {
    if (
      !payload.headers.authorization ||
      payload.headers.authorization.split(" ")[0] !== "Bearer"
    )
      return cb({message: "Unauthorised"}, HTTPCode.UNAUTHORISED);
    const token = payload.headers.authorization.split(" ")[1];

    const isTokenVerified = verifyJWT(token);

    if (!isTokenVerified)
      return cb({message: "Invalid token"}, HTTPCode.UNAUTHORISED);

    cb({message: "success"}, 200);
  }
}

module.exports = AuthHandler;
