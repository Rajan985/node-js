const HTTPCode = require("../constants/HTTPCode");

module.exports = (payload, cb) => {
  cb(
    {
      message: `${payload.method} /${payload.endPoint} does not exist in this server.`,
    },
    HTTPCode.NOT_FOUND
  );
};
