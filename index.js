const http = require("http");

const port = process.env.PORT || "dev";
const appEnv = process.env.NODE_ENV || 3000;

const requestHandler = require("./helpers/requestHandler");

const server = http.createServer(requestHandler);

server.listen(port, () => {
  console.info(`Server is up at http://localhost:${port}`);
  console.info(`App running in ${appEnv} environment   `);
});
