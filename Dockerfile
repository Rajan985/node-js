FROM node:20.10.0-bullseye

WORKDIR /app

COPY . /app

COPY .env /app

RUN npm install

EXPOSE 3000

CMD [ "npm", "run", "dev:start" ]