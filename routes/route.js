const AuthHandler = require("../handlers/auth.handler");
const DB = require("../DBserver");

const authHandler = new AuthHandler(DB);

module.exports = {
  "api/signup": authHandler.signup.bind(authHandler),
  "api/login": authHandler.login.bind(authHandler),
};
