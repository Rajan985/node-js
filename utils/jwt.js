const crypto = require("crypto");
const {publicKey, privateKey} = require("../constants/jwtKeys");

const base64Encoder = function (plainData) {
  return Buffer.from(JSON.stringify(plainData)).toString("base64");
};
const JWTheader = base64Encoder({alg: "RS256", type: "JWT"});

exports.signJWT = function (payload) {
  const JWTpayload = base64Encoder({
    ...payload,
    iat: Date.now() / 1000,
    exp: Date.now() / 1000 + 5 * 60,
  });

  const data = Buffer.from(`${JWTheader}.${JWTpayload}`);

  let signature = crypto.sign("sha256", data, {
    key: privateKey,
    padding: crypto.constants.RSA_PKCS1_PADDING,
  });

  signature = signature.toString("base64");

  return `${JWTheader}.${JWTpayload}.${signature}`;
};

exports.verifyJWT = function (token) {
  const splittedToken = token.split(".");
  let isValidToken;

  const payload = JSON.parse(
    Buffer.from(splittedToken[1], "base64").toString("utf-8")
  );

  isValidToken = payload.exp > Date.now() / 1000 ? true : false;

  const data = Buffer.from(`${splittedToken[0]}.${splittedToken[1]}`);

  const signature = Buffer.from(splittedToken[2], "base64");

  const isVerified = crypto.verify(
    "sha256",
    data,
    {
      key: publicKey,
      padding: crypto.constants.RSA_PKCS1_PADDING,
    },
    signature
  );

  isValidToken = isValidToken && isVerified === true ? true : false;

  return isValidToken;
};
