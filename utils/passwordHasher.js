const crypto = require("crypto");

exports.hashPassword = (password, salt) => {
  return crypto
    .scryptSync(password, salt, 64, {
      blockSize: 10,
      cost: 16384,
      parallelization: 3,
    })
    .toString("hex");
};
