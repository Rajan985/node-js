const {StringDecoder} = require("string_decoder");
const url = require("url");
const route = require("../routes/route");
const notFoundHandler = require("../handlers/notFound.handler");
const EndpointMethod = require("./methodAndEndpointMap");

module.exports = (req, res) => {
  const uri = url.parse(req.url, true);

  let completeEndpoint = uri.pathname.trim().replace("/", "");
  const endPoint =
    completeEndpoint.slice(-1) === "/"
      ? (completeEndpoint = completeEndpoint.slice(
          0,
          completeEndpoint.length - 1
        ))
      : completeEndpoint;

  const headers = req.headers;

  const method = req.method.toUpperCase();

  const queryObject = uri.query;

  const stringDecoder = new StringDecoder("utf8");

  let requestPayload = "";

  req.on("data", (chunck) => {
    requestPayload += stringDecoder.write(chunck);
  });

  req.on("end", () => {
    requestPayload += stringDecoder.end();

    const handlerPayload = {
      endPoint,
      headers,
      method,
      queryObject,
      requestPayload:
        requestPayload.length > 0 &&
        typeof JSON.parse(requestPayload) === "object"
          ? JSON.parse(requestPayload)
          : {},
    };

    const handler =
      route[endPoint] !== undefined && EndpointMethod.get(endPoint) === method
        ? route[endPoint]
        : notFoundHandler;

    handler(handlerPayload, (response, statusCode) => {
      res.setHeader("Content-Type", "application/json");
      res.writeHead(statusCode);
      res.end(JSON.stringify(response));
    });
  });
};
