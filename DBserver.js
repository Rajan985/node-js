const {MongoClient} = require("mongodb");

const client = new MongoClient(process.env.DB_URL);

const dbName = process.env.DB_NAME;

// async function main() {
//   await client.connect();
//   console.log("DB Connected successfully.");
//   const db = client.db(dbName);

//   return db;
// }

client.connect().then((res) => {
  console.log("DB Connected successfully.");
});

module.exports = client.db(dbName);

// module.exports = main();
